# LP TSN scheduler

## Descripción
Modeling of IEEE-802.1Qbv by heuristics and integer linear programming.

Given a network topology and a set of streams, our system generates the GCLs of the egress ports.
This code includes:
-   `genera_escenario.py`. Generates a random scenario consisting of network topology and streams.
-   `genera_ilp.py`. Generates the ILP model for the *lp_solve* solver from the scenario.
-   `flow.py`. Contains a class for streams.
-   `ajusta_windows.sh`. Processes the output of the solver and calls to `ajusta_windows.py`, which allocates gates to windows/frames.

## Installation
To run this software, [lp_solve](https://lpsolve.sourceforge.net/) must be installed.

## Usage
To generate and solve a scenario with 20 streams: ```python3 genera_escenario.py 20 > escenario.py && echo scenario ok && python3 genera_ilp.py > model.lp && echo model ok && lp_solve model.lp > model.result && echo solution ok && ./ajusta_windows.sh < model.result```

## Authors and acknowledgment
If you use this code, please provide the following reference:

A. Galilea Torres-Macías, J. Segarra Flor, J. L. Briz Velasco, A. Ramírez-Treviño and H. Blanco-Alcaine, "Fast IEEE802.1Qbv Gate Scheduling Through Integer Linear Programming," in IEEE Access, vol. 12, pp. 111239-111250, 2024, doi: 10.1109/ACCESS.2024.3440828.

```
@ARTICLE{10630823,
  author={Galilea Torres-Macías, Alitzel and Segarra Flor, Juan and Briz Velasco, José Luis and Ramírez-Treviño, Antonio and Blanco-Alcaine, Héctor},
  journal={IEEE Access}, 
  title={Fast IEEE802.1Qbv Gate Scheduling Through Integer Linear Programming}, 
  year={2024},
  volume={12},
  number={},
  pages={111239-111250},
  keywords={Streams;Scheduling;Bridges;Jitter;802.1Qbv;delay;GCL;ILP;jitter;real-time;scheduling;TAS;time-sensitive networking;TSN},
  doi={10.1109/ACCESS.2024.3440828}
  }
```

## License
This code is licensed under *GNU General Public License* (version 3) **WITH THE FOLLOWING RESTRICTION**:

*The use, investigation or development, in a direct or indirect way, of any of the scientific contributions of the authors contained in this work by any army or armed group in the world, for military purposes and for any other use which is against human rights or the environment, is strictly prohibited unless written consent is obtained from all the authors of this work, or all the people in the world.*
