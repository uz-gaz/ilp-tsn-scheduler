import sys
import csv

AVAILABLE_GATES = 8
#AVAILABLE_GATES = 99 # to avoid warning and get the gates that would be needed

#f = open('trafico')
#reader = csv.reader(f, delimiter=' ')
reader = csv.reader(sys.stdin, delimiter=' ')
# flow frame switch port complete ready
#data = [(int(col1), int(col2), int(col3), int(col4), float(col5), float(col6)) for col1, col2, col3, col4, col5, col6 in reader]
# flow frame switch port ready open complete close
data = [(int(col1), int(col2), int(col3), int(col4), float(col5), float(col6),float(col7), float(col8)) for col1, col2, col3, col4, col5, col6, col7, col8 in reader]

structured_data = {} # new dictionary for switch id.
#for (flow, frame, switch, port, complete, ready) in data:
for (flow, frame, switch, port, ready, wopen, complete, wclose) in data:
    if switch not in structured_data.keys():
        structured_data[switch] = {} # new dictionary for port id.
    if port not in structured_data[switch].keys():
        structured_data[switch][port] = [] # new list for switch+port
    # store all data, and the eventual gate number (-1)
    structured_data[switch][port].append((flow, frame, ready, wopen, complete, wclose, -1))

for switch in structured_data:
    for port in structured_data[switch]:
        structured_data[switch][port].sort(key=lambda tup: tup[2]) # ordenado por ready
        list_index = 0
        last_gate = -1 
        frames_using_port = len(structured_data[switch][port])
        max_gates = 0
        for current in range(frames_using_port):
            #(curr_flow, curr_frame, curr_ready, curr_complete, curr_gate) = structured_data[switch][port][current]
            (curr_flow, curr_frame, curr_ready, curr_open, curr_complete, curr_close, curr_gate) = structured_data[switch][port][current]
            # calculate the number of used gates until now:
            # prev_ready < curr_ready AND prev_complete > curr_ready
            used_gates = set()
            queued_flows = set()
            gate_to_reuse = -1
            for previous in range(current-1, 0, -1):
                #(prev_flow, prev_frame, prev_ready, prev_complete, prev_gate) = structured_data[switch][port][previous]
                (prev_flow, prev_frame, prev_ready, prev_open, prev_complete, prev_close, prev_gate) = structured_data[switch][port][previous]
                if prev_complete > curr_ready:
                    if prev_flow not in queued_flows:
                        queued_flows.add(prev_flow) # add flow
                        if prev_flow == curr_flow:
                            gate_to_reuse = prev_gate # reuse gate
                        else:
                            used_gates.add(prev_gate)
            # assign the gate number
            if curr_flow in queued_flows: # no new gate required
                if prev_gate in used_gates:
                    print("Warning: flow", curr_flow, "not isolated.")
                structured_data[switch][port][current] = (curr_flow, curr_frame, curr_ready, curr_open, curr_complete, curr_close, gate_to_reuse) # set gate number
                print("switch", switch, "port", port, "flow", curr_flow, "frame", curr_frame, "(re) using gate", gate_to_reuse, "; queue from", curr_ready, "to", curr_complete, "and window from", curr_open, "to", curr_close)
            else:
                # test if there are gates available
                if len(used_gates) < AVAILABLE_GATES:
                    gate = (last_gate + 1) % AVAILABLE_GATES # Round-Robin of gates
                    while gate in used_gates:
                        gate = (gate + 1) % AVAILABLE_GATES # Round-Robin of gates
                    structured_data[switch][port][current] = (curr_flow, curr_frame, curr_ready, curr_open, curr_complete, curr_close, gate) # set gate number
                    last_gate = gate
                    print("switch", switch, "port", port, "flow", curr_flow, "frame", curr_frame, "using gate", gate, "; queue from", curr_ready, "to", curr_complete, "and window from", curr_open, "to", curr_close)
                    if len(used_gates) + 1 > max_gates:
                        max_gates = len(used_gates) + 1
                else:
                    print("Warning: flow", curr_flow, "not isolated. Switch", switch, "requires more than", AVAILABLE_GATES, "gates in port", port)
                    gate = (last_gate + 1) % AVAILABLE_GATES # Round-Robin of gates
                    structured_data[switch][port][current] = (curr_flow, curr_frame, curr_ready, curr_open, curr_complete, curr_close, gate) # set gate number
                    last_gate = gate
                    #sys.exit(-1)
                    print("switch", switch, "port", port, "flow", curr_flow, "frame", curr_frame, "using gate", gate, "; queue from", curr_ready, "to", curr_complete, "and window from", curr_open, "to", curr_close)

        print(max_gates, "gates required in switch", switch, "port", port, "for", frames_using_port, "frames (1 frame/window)")

#print(structured_data)
