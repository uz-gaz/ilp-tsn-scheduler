# coding=utf-8

import sys

propagation_time = 1 # microseconds
NETWORK_SPEED = 1000 # bits/microsecond
SYNC_TRANSFORM = 81/80 # (1000Base-T: 10/8; 1000Base-T1: 81/80 ; Fast ethernet: 4B5B)

class Flow:
    #frame_size = 0  # Bytes
    trans_time = -1 # bits/microsecond
    period = 0  # microseconds/(frame ready)
    nodes = []      # list of nodes (tuples <switch, port/next switch>)
    start_node = -1 # starting node (first node of .nodes)
    end_nodes = {}  # set of ending nodes (one for unicast and several for multicast)
    e2e_time = 0    # end-to-end delay (microseconds)
    weight = 1      # weight for WFQ
    def __init__(self, t_time, period, node_list, end2end_time):
        # each node is a tuple (node, port), where port == node connected to
        #self.frame_size = size
        #self.trans_time = size * 8 * SYNC_TRANSFORM / NETWORK_SPEED
        self.trans_time = t_time # 13 used by TSNSched and Craciunas
        self.period = period
        if period < t_time:
            print("Error in flow parameters: cannot transmit a frame each %s microseconds if each frame takes %s to be transmitted" % (rate, t_time), file=sys.stderr)
            sys.exit(-2)
        self.nodes = node_list
        self.start_node = node_list[0]
        self.end_nodes = set(node_list)
        for node in node_list:
            for node2 in node_list:
                if node[1] == node2[0]: # link from node to node2
                    self.end_nodes.remove(node)
                    break
        self.e2e_time = end2end_time

    def print(self):
        #print("/* Frame size:", self.frame_size, "bytes */")
        print("/* Frame period:", self.period, "microseconds */")
        print("/* Frame transmission time:", self.trans_time, "microseconds */")
        print("/* Traversed nodes:", self.nodes, "*/")
        print("/* Start node:", self.start_node, "*/")
        print("/* End nodes:", self.end_nodes, "*/")
        print("/* Required end-to-end time:", self.e2e_time, "microseconds */")
        print("/* Weight for WFQ:", self.weight, "*/")

