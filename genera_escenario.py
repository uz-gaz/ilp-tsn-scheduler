#!angr/bin/python3
# coding=utf-8

""" Module with a set of functions to generate a TSN topology+flows scenario

"""

import sys
from random import randrange

SCENARIO_TYPE = "craciunas large" # DOI 10.1109/RTAS.2018.00008
#SCENARIO_TYPE = "HERMES small" # DOI 10.1145/3534879.3534906
#SCENARIO_TYPE = "personalized"

def main():
    #lp_file = sys.stdout
    endnodes_offset = 1000 # endnode ids will be offset+0, offset+1, etc.
    if SCENARIO_TYPE == "personalized":
        if len(sys.argv) != 3:
            print("usage for personalized scenario:", sys.argv[0], "<numSwitches> <numFlows>")
            sys.exit()
        num_switches = int(sys.argv[1])
        num_flows = int(sys.argv[2])
        endnodes_per_switch = 5 # x endnodes per switch (Craciunas: 3, 5)
    if SCENARIO_TYPE == "craciunas large":
        num_switches = 10
        if len(sys.argv) != 2:
            print("usage for personalized scenario:", sys.argv[0], "<numFlows>")
            sys.exit()
        num_flows = int(sys.argv[1])
        endnodes_per_switch = 5 # x endnodes per switch (Craciunas: 3, 5)
    elif SCENARIO_TYPE == "HERMES small":
        num_switches = 1
        endnodes_per_switch = 3
        if len(sys.argv) != 2:
            print("usage for HERMES scenario:", sys.argv[0], "<numFlows>")
            sys.exit()
        num_flows = int(sys.argv[1]) # ?? HERMES: Max number of generated frames is 100 but may be different --> 100 considering 2 links would be 50 flows
        # 30 flows ~ 10% utilization
        # 50 flows ~ 20% utilization
        # 60 flows ~ 25% utilization
        # 190 flows ~ 70% utilization
        # 240 flows ~ 93% utilization
        # 250 flows ~ 95% utilization
    else:
        print("ERROR: Scenario type not defined")
        sys.exit(-1)
    num_endnodes = num_switches * endnodes_per_switch

    print("from flow import Flow")
    print("def set_data_to_schedule():")
    print("    flow = []")
    for flow in range(0, num_flows):
        speaker = randrange(num_endnodes)
        listener1 = randrange(num_endnodes)
        while speaker == listener1:
            listener1 = randrange(num_endnodes)
        nodes = []      # list of nodes (tuples <switch, port/next switch>)
        first_switch = int(speaker / endnodes_per_switch)
        nodes.append((speaker + endnodes_offset, first_switch))
        # for each listener (add more listeners if required):
        last_switch1 = int(listener1 / endnodes_per_switch)
        nodes.append((last_switch1, listener1 + endnodes_offset))
        if first_switch < last_switch1: # any last switch
            distance = last_switch1 - first_switch + 2
            prev_switch = first_switch
            while prev_switch != last_switch1: # not in set of last_switches
                next_switch = prev_switch + 1
                nodes.append((prev_switch, next_switch))
                prev_switch = next_switch
        elif first_switch > last_switch1: # WARNING: with several last_switch, it is an 'if', not an 'elif'
            distance = first_switch - last_switch1 + 2
            prev_switch = first_switch
            while prev_switch != last_switch1: # not in set of last_switches
                next_switch = prev_switch - 1
                nodes.append((prev_switch, next_switch))
                prev_switch = next_switch
        else: # first_switch == last_switch 
            distance = 2
        # parameters: transmission_time, flow_period, node_list, end2end_time
        # 13 microseconds > (MTU bytes * 8 bits/byte *syncFactor / 1 Gbit/s)
        # flow_period: time between start sending each frame
        #flow_period = 2000 # TSNSched (2000, 1000, 500)
        if SCENARIO_TYPE == "craciunas large":
            transmission_time = 13 # 13 used by TSNSched and Craciunas
            flow_period = 10000 + 10000*randrange(2) #(flow%2) # (10-20ms) Serna Oliver, Craciunas
            end2end_time = flow_period
        elif SCENARIO_TYPE == "HERMES small":
            transmission_time = randrange(3,7) # HERMES: 500-1000 bytes
            flow_period = randrange(200, 1000, 200) # 200-1000 (steps of 200)
            end2end_time = flow_period
        #flow_period = 10000 # para crear casos no planificables
        #flow_period = 15*(2+flow)*(2+flow-1) # sum_{n=2->inf} 1/(n*(n-1)) = 1
        # end2end_time: distance * (
        #               transmission_time(13) +
        #               propagation_time(1) +
        #               frame_processing_time(0)
        #               ) + margin
        #propagation_time = 1
        #frame_processing_time = 0
        #margin = 1
        #end2end_time = distance*(transmission_time+propagation_time+frame_processing_time)+margin
        print("    flow.append(Flow(", transmission_time , ",", flow_period, ",", nodes, ",", end2end_time, "))")
    print("    return flow")


if __name__ == "__main__":
    sys.exit(main())
