# coding=utf-8

""" Module with a set of functions to build an lp_solve lp file

Restrictions: 
- Each frame in a switch egress port has an associated window to be sent,
that is, there are as much windows as frames to be transmitted from a switch egress port.
- Windows of egress ports are preordered using Fair Queueing.
This may not be the optimal ordering.
- Each flow has an associated gate, that is, there are as much gates as flows.
Since the number of gates should not be greater than 8, after obtaining the
results, gates should be reasigned from gate 0 to 7 (or max-gate), for instance
using Round-Robin.

0 <= flowOffset <= flowFramerate
frameReadyFirstSwitch = flowOffset + numFrame*frameRate
frameOffsetInSwitch = frameReadyFirstSwitch + frameDelayInSwitch

frameStartInSwitch = frameOffsetInSwitch + sum(numPeriod*whichPeriod)

0 <= gateXopen <= flowXframe0start <=  flowXframe0complete <= gateXclose
"""

import sys
import time
from math import lcm, ceil
from flow import Flow
from escenario import set_data_to_schedule


propagation_time = 0 # Time required for the signal to travel through physical links (microseconds). Depends on the cable length and features

clock_max_error = 0 # Max error that a bridge clock may have between time syncrhonizations (microseconds)

processing_time = 0 # Time required to process a frame in the bridge: from ingress port to egress queue.

#NETWORK_SPEED = 1000 # bits/microsecond
#SYNC_TRANSFORM = 81/80 # (1000Base-T: 10/8; 1000Base-T1: 81/80 ; Fast ethernet: 4B5B)
#
#class Flow:
#    frame_size = 0  # Bytes
#    trans_time = -1 # bits/second
#    period = 0  # frames/second
#    nodes = []      # list of nodes (tuples <switch, port/next switch>)
#    start_node = -1 # starting node (first node of .nodes)
#    end_nodes = {}  # set of ending nodes (one for unicast and several for multicast)
#    e2e_time = 0    # end-to-end delay
#    weight = 1      # weight for WFQ
#    def __init__(self, size, rate, node_list, end2end_time):
#        # each node is a tuple (node, port), where port == node connected to
#        self.frame_size = size
#        #self.trans_time = size * 8 * SYNC_TRANSFORM / NETWORK_SPEED
#        self.trans_time = 13 # value used by TSNSched and Craciunas
#        self.period = rate
#        self.nodes = node_list
#        self.start_node = node_list[0]
#        self.end_nodes = set(node_list)
#        for node in node_list:
#            for node2 in node_list:
#                if node[1] == node2[0]: # link from node to node2
#                    self.end_nodes.remove(node)
#                    break
#        self.e2e_time = end2end_time
#
#    def print(self):
#        print("/* Frame size:", self.frame_size, "bytes */")
#        print("/* Frame rate:", self.period, "microsecond/frame */")
#        print("/* Frame transmission time:", self.trans_time, "microseconds */")
#        print("/* Traversed nodes:", self.nodes, "*/")
#        print("/* Start node:", self.start_node, "*/")
#        print("/* End nodes:", self.end_nodes, "*/")
#        print("/* Required end-to-end time:", self.e2e_time, "seconds */")
#        print("/* Weight for WFQ:", self.weight, "*/")


def genera_datos_iniciales():
    """ Function to set initial test flows """
    propagation_time = 1 # microseconds
    NETWORK_SPEED = 100 # bits/microsecond
    SYNC_TRANSFORM = 5/4 # (Fast ethernet: 4B5B)
    # TODO: add WFQ parameter to flows
    flow = []
    # size (Bytes), frame rate (frames/microsecond), route, end2end (microsec)
    flow.append(Flow(10, 5, [(0,1), (1,2), (1,3)], 9))
    flow.append(Flow(20, 10, [(0,1), (1,2)], 12))
    flow.append(Flow(10, 10, [(1,3)], 12))
    return flow


# Functions to generate the names of the variables for lp_solve
def flow_time_margin(flow):
    """ Flow time margin: e2e-delay minus time of last transmission and propagation from last node (any frame) """
    return "st%dtimeMargin" % (flow)
def frame_time_margin(flow, frame):
    """ Frame time margin: e2e-delay minus time of last transmission and propagation from last node of a given frame """
    return "st%dfr%dtimeMargin" % (flow, frame)
def flow_average_delay(flow):
    """ Average delays of frames in the flow """
    return "st%davgDelay" % (flow)
def frame_delay(flow, frame):
    """ Delay of a given frame """
    return "st%dfr%ddelay" % (flow, frame)
def flow_jitter(flow):
    """ Max. jitter of the flow """
    return "st%djitter" % (flow)
def flow_offset(flow):
    """ Flow start offset with respect to time 0 """
    return "st%doffset" % (flow)
def frame_jitter(flow, frame):
    """ Jitter of a given frame """
    return "st%dfr%djitter" % (flow, frame)
def frame_node_complete(flow, frame, switch):
    """ Time when the frame is completely transmitted from switch/port """
    return "st%dfr%dsw%dpt%dcomplete" % (flow, frame, switch[0], switch[1])
def frame_node_start(flow, frame, switch):
    """ Time when the frame starts its transmission from switch/port """
    return "st%dfr%dsw%dpt%dstart" % (flow, frame, switch[0], switch[1])
def frame_node_ready(flow, frame, switch):
    """ Time when the frame is ready to start its transmission at first switch """
    return "st%dfr%dsw%dpt%dready" % (flow, frame, switch[0], switch[1])
def frame_node_offset(flow, frame, switch):
    """ Transmission start offset with respect to the hyperperiod """
    return "st%dfr%dsw%dpt%doffset" % (flow, frame, switch[0], switch[1])
def frame_node_delay(flow, frame, switch):
    return "st%dfr%dsw%dpt%ddelay" % (flow, frame, switch[0], switch[1])
def frame_node_window(flow, frame, switch, period):
    """ Transmission window when frame is transmitted from switch """
    return "st%dfr%dsw%dpt%dwindow%dhp%d" % (flow, frame, switch[0], switch[1], frame, period)
def node_gate_open_offset(flow, node, window):
    """ Offset of opening window in gate """
    return "sw%dpt%dgt%dw%dopenOffset" % (node[0], node[1], flow, window)
def node_gate_close_offset(flow, node, window):
    """ Offset of closing window in gate """
    return "sw%dpt%dgt%dw%dcloseOffset" % (node[0], node[1], flow, window)
def node_gate_pregap_open_offset(flow, node, window):
    """ Offset of gap (no window open) open in gate """
    return "sw%dpt%dgt%dw%dgapOpenOffset" % (node[0], node[1], flow, window)
def node_gate_pregap_close_offset(flow, node, window):
    """ Offset of gap (no window open) close in gate """
    return "sw%dpt%dgt%dw%dgapCloseOffset" % (node[0], node[1], flow, window)
def node_gate_gap(node):
    """ Global variable to accumulate gaps in switch/port """
    return "sw%dpt%dgap" % (node[0], node[1])



def calculate_hyperperiod(flow):
    """ Returns the hyperperiod of the frame rates in flows

    Hyperperiod is calculated as the least common multiple of the frame
    rates of the flows.
    """
    hyperperiod = flow[0].period
    for num_flow in range(1, len(flow)):
        hyperperiod = lcm(flow[num_flow].period, hyperperiod)
    return hyperperiod


def flow_max_hops(flow):
    """ Returns the maximum number of hops in a flow """
    max_hops = 0
    for endnode in flow.endnodes:
        max_hops = max(max_hops, node_order(endnode, flow))
    return max_hops

def node_order(node, flow):
    """ Returns the number of nodes traversed by this flow before reaching node 
    
    Multicast flows are supported.
    Multipath flows (those that reach a switch from different paths to provide
    failure tolerance) may return the node order of any path, that is, any path
    should have the same number of hops to be consistent.
    TODO: return the longest! path in multipath flows
    """
    assert node in flow.nodes
    hops = 0
    previous = node
    while previous != flow.start_node:
        for flow_node in flow.nodes:
            if flow_node[1] == previous[0]:
                hops += 1
                previous = flow_node
                break
    return hops


def calculate_flow_order(hyperperiod, flow, node_set):
    """ Returns the sending order of frames in a Fair Queueing policy
    assuming that all of them are ready at instant 0

    THE RESULTING SCHEDULING WILL SCHEDULE THE ORDER OF OPENING
    WINDOWS IN GATES IN THIS ORDER, WHICH MAY NOT BE THE BEST ONE
    """
    # calculate WFQsum (weight=flow_weight/WFQsum)
    WFQsum = {}
    for node in node_set: # for all nodes in the network
        WFQsum[node] = 0
        for num_flow in range(0, len(flow)):
            if node in flow[num_flow].nodes:
                WFQsum[node] += flow[num_flow].weight
    # calculate flow order
    frame_index = []
    flow_order = {}
    for node in node_set: # for all nodes in the network
        # calculate transmission complete times for each frame in this node
        flow_completion_times = {} # array of flows, each flow being a list of <frame,completion_time>
        for num_flow in range(0, len(flow)):
            frame_index.insert(num_flow, 0) # set starting index for the while loop
            if node in flow[num_flow].nodes:
                flow_completion_times[num_flow] = [] # empty list of frame completion times
                hops_to_node = node_order(node, flow[num_flow])
                for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
                    frame_ready = num_frame * flow[num_flow].period
                    transmission_times = (hops_to_node+1) * flow[num_flow].trans_time * (flow[num_flow].weight/WFQsum[node])
                    #transmission_times = (hops_to_node+1) * flow[num_flow].trans_time
                    propagation_times = hops_to_node * (propagation_time+processing_time)
                    flow_completion_times[num_flow].insert(num_frame, frame_ready + transmission_times + propagation_times)
        # calculate flow/frame order according to previous transmission complete times
        flow_order[node] = []
        found = True
        while found:
            min_time = 9999999 # All times should be lower than this value
            found = False
            # Search for the lowest time from the frames not yet ordered
            for num_flow in range(0, len(flow)):
                if node in flow[num_flow].nodes:
                    if frame_index[num_flow] < len(flow_completion_times[num_flow]):
                        # if there are frames not yet ordered in this flow
                        if flow_completion_times[num_flow][frame_index[num_flow]] < min_time:
                            # found a time lower than the current one, anotate this time/flow/frame
                            found = True
                            min_time = flow_completion_times[num_flow][frame_index[num_flow]]
                            min_time_flow = num_flow
                            min_time_frame = frame_index[num_flow]
            if found:
                # Add the frame found and increase the frame index to avoid exploring it again
                flow_order[node].append((min_time_flow, min_time_frame))
                frame_index[min_time_flow] += 1
    return flow_order


def generate_offset_constraints(lp_file, flow):
    lp_file.write("\n/* Flow offset constraints (for each flow) */\n\n")
    lp_file.write("/* 0 <= flow_offset (default unless free variable)*/\n")
    lp_file.write("/* flow_offset < flow_period */\n")
    for num_flow in range(0, len(flow)):
            #lp_file.write("0 <= %s;\n" % (flow_offset(num_flow)))
            lp_file.write("%s < %d;\n" % (flow_offset(num_flow), flow[num_flow].period))


def generate_first_ready_constraints(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Frame ready constraints from source end-station (for each frame) */\n\n")
    lp_file.write("/* frame_ready = flow_period*num_frame + flow_offset */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            lp_file.write("%s = %d + %s;\n" % (frame_node_ready(num_flow, num_frame, flow[num_flow].start_node), num_frame*flow[num_flow].period, flow_offset(num_flow)))


def generate_frame_start_constraints(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Frame start in first switch constraints (for each frame) */\n\n")
    lp_file.write("/* frame_ready <= frame_start */\n")
    for num_flow in range(0, len(flow)):
        for src_node in flow[num_flow].nodes:
            for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
                #lp_file.write("%s <= %s;\n" % (frame_node_ready(num_flow, num_frame, flow[num_flow].start_node), frame_node_start(num_flow, num_frame, flow[num_flow].start_node)))
                lp_file.write("%s <= %s;\n" % (frame_node_ready(num_flow, num_frame, src_node), frame_node_start(num_flow, num_frame, src_node)))


def generate_frame_completion_constraints(lp_file, hyperperiod, flow):
    """ Write frame completion constraints """
    lp_file.write("\n/* Frame completion constraints */\n\n")
    lp_file.write("/* flowXframeYswitchZcomplete = flowXframeYswitchZstart +  flowXframeYtransmissionTime */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            for num_node in flow[num_flow].nodes:
                lp_file.write("%s = %s + %d;\n" % (frame_node_complete(num_flow, num_frame, num_node), frame_node_start(num_flow, num_frame, num_node), flow[num_flow].trans_time))


def generate_path_constraints(lp_file, hyperperiod, flow):
    """ el inicio en switch 2 debe ser mayor que el fin en 1 + tprop"""
    lp_file.write("\n/* Path completion constraints */\n\n")
    lp_file.write("/* flowXframeYswitchZcomplete + (propagation_time+processing_time) <= flowXframeYswitchZNEXTready */\n")
    for num_flow in range(0, len(flow)):
        for src_node in flow[num_flow].nodes:
            for dst_node in flow[num_flow].nodes:
                if src_node[1] == dst_node[0]:
                    for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
                        lp_file.write("%s + %d <= %s;\n" % (frame_node_complete(num_flow, num_frame, src_node), propagation_time+processing_time, frame_node_ready(num_flow, num_frame, dst_node)))


def generate_frame_order_constraints(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Frame order constraints */\n\n")
    lp_file.write("/* flowXframeYswitchZcomplete <= flowXframeY+1switchZstart */\n")
    for num_flow in range(0, len(flow)):
        for num_node in flow[num_flow].nodes:
            for num_frame in range(1, int(hyperperiod/flow[num_flow].period)):
                lp_file.write("%s <= %s;\n" % (frame_node_complete(num_flow, num_frame-1, num_node), frame_node_start(num_flow, num_frame, num_node)))


##################################################
# Gate constraints
##################################################

def generate_gate_constraints(lp_file, flow, switch_period, node_set, flow_order):
    lp_file.write("\n/* Gate open/close constraints (ordered gates) */\n\n")
    lp_file.write("/* 0 == gateFirstgapOpenOffset */\n")
    lp_file.write("/* gateFirstgapCloseOffset = gateXopenOffset */\n")
    lp_file.write("/*   gateXopenOffset <= gateXcloseOffset */\n")
    lp_file.write("/*   gateXcloseOffset = gateXgapOpenOffset */\n")
    lp_file.write("/*   gateXgapOpenOffset <= gateXgapCloseOffset */\n")
    lp_file.write("/*   gateXgapCloseOffset = gateX+1openOffset */\n")
    lp_file.write("/* gateLastopenOffset <= gateLastcloseOffset */\n")
    lp_file.write("/* gateLastcloseOffset = gateLastgapOpenOffset */\n")
    lp_file.write("/* gateLastgapOpenOffset <= gateLastgapCloseOffset */\n")
    lp_file.write("/* gateLastgapCloseOffset = Hyperperiod */\n")

    # num_gate equivalent to num_flow
    for num_node in node_set:
        first = True
        for (num_flow, num_frame) in flow_order[num_node]:
            # X <= gap open
            if first:
                first = False
                lp_file.write("0 = %s;\n" % node_gate_pregap_open_offset(num_flow, num_node, num_frame)) # 0 = gap open
            else:
                lp_file.write("%s = %s;\n" % (node_gate_close_offset(previous_flow, num_node, previous_frame), node_gate_pregap_open_offset(num_flow, num_node, num_frame))) # previous gate close = gap open
            # gap open <= gap close
            lp_file.write("%s <= %s;\n" % (node_gate_pregap_open_offset(num_flow, num_node, num_frame), node_gate_pregap_close_offset(num_flow, num_node, num_frame)))
            # gap close = gate open
            lp_file.write("%s = %s;\n" % (node_gate_pregap_close_offset(num_flow, num_node, num_frame), node_gate_open_offset(num_flow, num_node, num_frame))) # gap close = current gate open
            previous_flow = num_flow
            previous_frame = num_frame
            # gate open <= gate close
            lp_file.write("%s <= %s;\n" % (node_gate_open_offset(num_flow, num_node, num_frame), node_gate_close_offset(num_flow, num_node, num_frame)))
            # gate close = gapFin open <= gapFin close = X
            if (num_flow, num_frame) == flow_order[num_node][len(flow_order[num_node])-1]:
                #lp_file.write("\n")
                # gate close = gapFin open
                lp_file.write("%s = %s;\n" % (node_gate_close_offset(num_flow, num_node, num_frame), node_gate_pregap_open_offset(num_flow+1, num_node, num_frame+1))) # previous gate close = gap open
                # gap open <= gap close
                lp_file.write("%s <= %s;\n" % (node_gate_pregap_open_offset(num_flow+1, num_node, num_frame+1), node_gate_pregap_close_offset(num_flow+1, num_node, num_frame+1)))
                # gapFin close = X
                lp_file.write("%s = %d;\n" % (node_gate_pregap_close_offset(num_flow+1, num_node, num_frame+1), switch_period))
                lp_file.write("\n")
            #lp_file.write("\n")

    lp_file.write("\n/* Gap calculation */\n\n")
    lp_file.write("/* brigeXportYgap = sum(bridgeXportYgaps) */\n")
    lp_file.write("/* maxgap = sum(bridgeXportYgap) */\n")
    for num_node in node_set:
        lp_file.write("%s =" % node_gate_gap(num_node))
        for (num_flow, num_frame) in flow_order[num_node]:
            lp_file.write(" + %s - %s" % (node_gate_pregap_close_offset(num_flow, num_node, num_frame), node_gate_pregap_open_offset(num_flow, num_node, num_frame)))
        lp_file.write(" + %s - %s;\n" % (node_gate_pregap_close_offset(num_flow+1, num_node, num_frame+1), node_gate_pregap_open_offset(num_flow+1, num_node, num_frame+1)))
    lp_file.write("maxgap =")
    for num_node in node_set:
        lp_file.write(" + %s" % node_gate_gap(num_node))
    lp_file.write(";\n")


##################################################
# Frame to window association
##################################################

def generate_frame_start_offset_constraints(lp_file, hyperperiod, periods, flow):
    lp_file.write("\n/* Frame start offset constraints */\n\n")
    lp_file.write("/* flowXframeYswitchZstart = flowXframeYswitchZoffset +  SUM( NUMwindow flowXframeYswitchZwindowNUMwindow ) */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            for num_node in flow[num_flow].nodes:
                lp_file.write("%s = %s" % (frame_node_start(num_flow, num_frame, num_node), frame_node_offset(num_flow, num_frame, num_node)))
                for num_period in range (0, periods):
                    lp_file.write(" + %d %s" % (num_period*hyperperiod, frame_node_window(num_flow, num_frame, num_node, num_period)))
                lp_file.write(";\n")
                #lp_file.write(" + %d;\n" % (num_frame*flow[num_flow].period))
    # always 1 window active
    lp_file.write("\n/* 1 = flowXframeYswitchZwindow0 + flowXframeYswitchZwindow1 +... */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            for num_node in flow[num_flow].nodes:
                lp_file.write("1 =")
                for num_period in range (0, periods):
                    #for num_window in range(0, int(num_periods * (hyperperiod/flow[num_flow].period))):
                    lp_file.write(" + %s" % (frame_node_window(num_flow, num_frame, num_node, num_period)))
                lp_file.write(";\n")


def generate_gate_transmission_start_constraints(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Gate to frame transmission start constraints */\n\n")
    lp_file.write("/* gateXopenOffset = flowXframeYswitchZstartoffset - MaxClockError */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            for num_node in flow[num_flow].nodes:
                lp_file.write("%s = %s - %d;\n" % (node_gate_open_offset(num_flow, num_node, num_frame) , frame_node_offset(num_flow, num_frame, num_node), clock_max_error))


def generate_gate_transmission_complete_constraints(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Gate to frame transmission complete constraints */\n\n")
    lp_file.write("/* flowXframeYswitchZstartoffset + flowXframeYtransmissionTime + MaxClockError = gateXcloseOffset */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            for num_node in flow[num_flow].nodes:
                lp_file.write("%s + %d + %d = %s;\n" % (frame_node_offset(num_flow, num_frame, num_node), flow[num_flow].trans_time, clock_max_error, node_gate_close_offset(num_flow, num_node, num_frame)))



##################################################
# Computation of objective, delays and jitter
##################################################

def generate_max_time_margin_objective(lp_file, hyperperiod, flow):
    """ Write objective for ILP """
    lp_file.write("\n/* Objective function */\n\n")
    lp_file.write("max: timeMargin;\n")
    #lp_file.write("max: timeMargin + maxgap;\n")
    #lp_file.write("max: timeMargin + maxgap - maxjitter;\n")

def generate_time_margin_constraints(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Time margin constraints */\n\n")
    lp_file.write("/* time_margin = flowXtime_margin + flowYtime_margin... */\n")
    lp_file.write("timeMargin =")
    for num_flow in range(0, len(flow)):
        lp_file.write(" + %s" % flow_time_margin(num_flow))
    lp_file.write(";\n")

    lp_file.write("\n/* flowXtime_margin <= flowXframeYtime_margin */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            lp_file.write("%s <= %s;\n" % (flow_time_margin(num_flow), frame_time_margin(num_flow, num_frame)))

    lp_file.write("\n/* flowXframeYtime_margin <= e2e_requirement - flowXframeYdelay */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            lp_file.write("%s = %d - %s;\n" % (frame_time_margin(num_flow, num_frame), flow[num_flow].e2e_time, frame_delay(num_flow, num_frame)))

def generate_frame_delay_constraints(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Frame delay constraints */\n\n")
    lp_file.write("/* frame_delay = frame_complete + (propagation_time+processing_time) - frame_offset */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            for end_node in flow[num_flow].end_nodes:
                lp_file.write("%s + %d - %s <= %s;\n" % (frame_node_complete(num_flow, num_frame, end_node), propagation_time+processing_time, frame_node_ready(num_flow, num_frame, flow[num_flow].start_node), frame_delay(num_flow, num_frame)))

def generate_jitter_constraints(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Jitter constraints */\n\n")
    lp_file.write("/* flowXaverage_delay = sum(flowXframeYdelay) / max_frames */\n")
    for num_flow in range(0, len(flow)):
        lp_file.write("%s =" % (flow_average_delay(num_flow)))
        max_frames = int(hyperperiod/flow[num_flow].period)
        for num_frame in range(0, max_frames):
            lp_file.write(" + %f %s" % (1/max_frames, frame_delay(num_flow, num_frame)))
        lp_file.write(";\n")

    lp_file.write("\n/* flowXaverage_delay - flowXframeYdelay <=  flowXframeYjitter */\n")
    lp_file.write("/* flowXframeYdelay - flowXaverage_delay <=  flowXframeYjitter */\n")
    for num_flow in range(0, len(flow)):
        max_frames = int(hyperperiod/flow[num_flow].period)
        for num_frame in range(0, max_frames):
            lp_file.write("%s - %s <= %s;\n" % (flow_average_delay(num_flow), frame_delay(num_flow, num_frame), frame_jitter(num_flow, num_frame)))
            lp_file.write("%s - %s <= %s;\n" % (frame_delay(num_flow, num_frame), flow_average_delay(num_flow), frame_jitter(num_flow, num_frame)))

    lp_file.write("\n/* flowXframeYjitter <=  flowXjitter */\n")
    for num_flow in range(0, len(flow)):
        max_frames = int(hyperperiod/flow[num_flow].period)
        for num_frame in range(0, max_frames):
            lp_file.write("%s <= %s;\n" % (frame_jitter(num_flow, num_frame), flow_jitter(num_flow)))

    lp_file.write("\n/* flowXjitter <=  maxjitter */\n")
    for num_flow in range(0, len(flow)):
        lp_file.write("%s <= maxjitter;\n" % (flow_jitter(num_flow)))


#def generate_frame_offset_constraints(lp_file, hyperperiod, flow):
#    lp_file.write("\n/* frame offset constraints */\n\n")
#    lp_file.write("/* flow_frame_offset = flow_offset + flowXperiod*Y + flow_frame_delay */\n")
#    for num_flow in range(0, len(flow)):
#        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
#            for node in flow[num_flow].nodes:
#                lp_file.write("%s = %s + %d + %s;\n" % (frame_node_offset(num_flow, num_frame, node), flow_offset(num_flow), num_frame*flow[num_flow].period, frame_node_delay(num_flow, num_frame, node)))

#def generate_frame_offset_constraints(lp_file, hyperperiod, flow):
#    lp_file.write("\n/* frame offset constraints */\n\n")
#    lp_file.write("/* flow_frame_offset = frame_ready_offset + flow_frame_delay */\n")
#    for num_flow in range(0, len(flow)):
#        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
#            for node in flow[num_flow].nodes:
#                lp_file.write("%s = %s + %s;\n" % (frame_node_offset(num_flow, num_frame, node), frame_node_ready_offset(num_flow, num_frame, node), frame_node_delay(num_flow, num_frame, node)))


# Not needed; included in flow offset constraints
#def generate_flow_start_constraints(lp_file, hyperperiod, flow):
#    """ Write flow start constraints """
#    lp_file.write("\n/* flow start constraints */\n\n")
#    lp_file.write("/* flowXperiod*Y <= flowXframeYswitchFIRSTstart */\n")
#    for num_flow in range(0, len(flow)):
#        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
#            for num_node in flow[num_flow].nodes:
#                lp_file.write("%d <= %s;\n" % (num_frame*flow[num_flow].period, frame_node_start(num_flow, num_frame, flow[num_flow].start_node)))

# OBSOLETE!!
def generate_flow_e2e_constraints(lp_file, hyperperiod, flow):
    """ Write flow e2e constraints """
    lp_file.write("\n/* flow end2end constraints */\n\n")
    lp_file.write("/* flowXframeYswitchLASTcomplete <= frameSTARToffset + flowXperiod*Y + end2end_delay */\n")
    for num_flow in range(0, len(flow)):
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            for end_node in flow[num_flow].end_nodes:
                lp_file.write("%s <= %s + %d;\n" % (frame_node_complete(num_flow, num_frame, end_node), frame_node_offset(num_flow, num_frame, flow[num_flow].start_node), num_frame*flow[num_flow].period + flow[num_flow].e2e_time))





#def generate_equivalent_gate_constraints(lp_file, flow):
#    lp_file.write("\n/* Force same open/close times in all gates */\n\n")
#
#    for num_flow in range(0, len(flow)):
#        for num_node in flow[num_flow].nodes:
#            if num_node != flow[0].start_node:
#                lp_file.write("%s = %s;\n" % (node_gate_open_offset(num_flow, flow[0].start_node), node_gate_open_offset(num_flow, num_node)))
#                lp_file.write("%s = %s;\n" % (node_gate_close_offset(num_flow, flow[0].start_node), node_gate_close_offset(num_flow, num_node)))

#def generate_gate_constraints(lp_file, flow, switch_period):
#    lp_file.write("\n/* Gate open/close constraints (ordered gates) */\n\n")
#    lp_file.write("/* gateXopenOffset <= gateXcloseOffset */\n")
#    lp_file.write("/* gateXcloseOffset <= gateX+1openOffset */\n")
#
#    # num_gate equivalent to num_flow
#    printed_nodes = set()
#    for num_flow in range(0, len(flow)):
#        for num_node in flow[num_flow].nodes:
#            if num_node not in printed_nodes:
#                printed_nodes.add(num_node)
#                for num_gate in range(0, len(flow)):
#                    # X <= gate open
#                    if num_gate == 0:
#                        lp_file.write("0 <= %s;\n" % node_gate_open_offset(num_gate, num_node))
#                    else:
#                        lp_file.write("%s <= %s;\n" % (node_gate_close_offset(num_gate-1, num_node), node_gate_open_offset(num_gate, num_node)))
#                    # gaten open <= gate close
#                    lp_file.write("%s <= %s;\n" % (node_gate_open_offset(num_gate, num_node), node_gate_close_offset(num_gate, num_node)))
#                    if num_gate == len(flow)-1:
#                        # gate close <= X
#                        lp_file.write("%s <= %d;\n" % (node_gate_close_offset(num_gate, num_node), switch_period))

#def generate_gate_constraints_orig(lp_file, flow, switch_period, node_set, flow_order):
#    # constraints without gaps
#    lp_file.write("\n/* Gate open/close constraints (ordered gates) */\n\n")
#    lp_file.write("/* gateXopenOffset <= gateXcloseOffset */\n")
#    lp_file.write("/* gateXcloseOffset <= gateX+1openOffset */\n")
#
#    # num_gate equivalent to num_flow
#    for num_node in node_set:
#        first = True
#        for (num_flow, num_frame) in flow_order[num_node]:
#            # X <= gate open
#            if first:
#                first = False
#                lp_file.write("0 <= %s;\n" % node_gate_open_offset(num_flow, num_node, num_frame))
#            else:
#                lp_file.write("%s <= %s;\n" % (node_gate_close_offset(previous_flow, num_node, previous_frame), node_gate_open_offset(num_flow, num_node, num_frame)))
#            previous_flow = num_flow
#            previous_frame = num_frame
#            # gaten open <= gate close
#            lp_file.write("%s <= %s;\n" % (node_gate_open_offset(num_flow, num_node, num_frame), node_gate_close_offset(num_flow, num_node, num_frame)))
#            # gate close <= X
#            if (num_flow, num_frame) == flow_order[num_node][len(flow_order[num_node])-1]:
#                lp_file.write("%s <= %d;\n" % (node_gate_close_offset(num_flow, num_node, num_frame), switch_period))




def generate_binary_variables(lp_file, hyperperiod, periods, flow, node_set, flow_order):
    if periods > 1:
        lp_file.write("\n/* Binary variables (hyperperiod where each frame is transmitted)*/\n\n")
        first_one = True
        for num_flow in range(0, len(flow)):
            for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
                for num_node in flow[num_flow].nodes:
                    for num_period in range (0, periods):
                        if first_one:
                            lp_file.write("bin %s" % frame_node_window(num_flow, num_frame, num_node, num_period))
                            first_one = False
                        else:
                            lp_file.write(", %s" % frame_node_window(num_flow, num_frame, num_node, num_period))
        lp_file.write(";\n")
    else:
        lp_file.write("\n/* Binary variables (hyperperiod where each frame is transmitted) not required */\n\n")


def generate_free_variables(lp_file, hyperperiod, flow):
    lp_file.write("\n/* Free variables */\n\n")
    lp_file.write("/* WARNING time_margins: solver will find a schedule even if some time margin is negative, that is, requires more end-to-end delay */\n")
    lp_file.write("free ")
    for num_flow in range(0, len(flow)):
        lp_file.write("%s, " % flow_time_margin(num_flow))
        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
            lp_file.write("%s, " % frame_time_margin(num_flow, num_frame))
    lp_file.write("timeMargin;\n")



def generate_xml_template(hyperperiod, flow):
    xml_file=open("template.xml", 'w')
    xml_file.write("<?xml version=\"1.0\"?>\n")
    xml_file.write("<tsn_schedule>\n")
    printed_nodes = set()
    for flow_seek_nodes in range(0, len(flow)):
        for node in flow[flow_seek_nodes].nodes:
            if node not in printed_nodes:
                printed_nodes.add(node)
                xml_file.write("  <switch id=\"%d\" port=\"%d\">\n" % (node[0], node[1]))
                for num_flow in range(0, len(flow)):
                    if node in flow[num_flow].nodes:
                        xml_file.write("    <gate id=\"%d\" open=\"\" close=\"\">\n" % (num_flow))
                        for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
                            xml_file.write("    <flow id=\"%d\" frame=\"%d\" gate=\"%d\" start=\"\" complete=\"\">\n" % (num_flow, num_frame, num_flow))
                xml_file.write("  </switch>\n")
    xml_file.write("</tsn_schedule>\n")
    xml_file.close()

def generate_latex_template(hyperperiod, flow, node_set, flow_order):
    latex_file=open("template.tex", 'w')
    latex_file.write("\\documentclass{article}\n\\usepackage{pgfgantt}\n\\begin{document}\n")
    latex_file.write("% OJO: hay que poner una unidad menos en FIN\n")
#    for num_flow in range(0, len(flow)):
#        for num_node in flow[num_flow].nodes:
#            latex_file.write("\\def\\%s{INTEGER}\n\\def\\%s{\\the\\numexpr INTEGER - 1 \\relax}\n" % (node_gate_open_offset(num_flow, num_node), node_gate_close_offset(num_flow, num_node)))
#            for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
#                latex_file.write("\\def\\%s{INTEGER}\n\\def\\%s{INTEGER}\n" % (frame_node_start(num_flow, num_frame, num_node), frame_node_complete(num_flow, num_frame, num_node)))
    latex_file.write("Hiperperiodo: %d (0--%d)\n\n" % (hyperperiod, hyperperiod-1))
    latex_file.write("\\input{valores.tex}\n")
    latex_file.write("% OJO: hay que poner una unidad menos en FIN\n")
    latex_file.write("\\definecolor{cola}{RGB}{251, 180, 174}\n\\definecolor{colb}{RGB}{179, 205, 227}\n\\definecolor{colc}{RGB}{204, 235, 197}\n\\definecolor{cold}{RGB}{222, 203, 228}\n\\definecolor{cole}{RGB}{254, 217, 166}\n\\definecolor{colf}{RGB}{255, 255, 204}\n\\definecolor{colg}{RGB}{229, 216, 189}\n\\definecolor{col0}{RGB}{225, 255, 255}\n")
    latex_file.write("\\begin{ganttchart}[inline,vgrid={*{%d}{dotted},dashed}]{0}{25}\n" % (hyperperiod-1))
    latex_file.write("\\gantttitlelist{0,...,25}{1}\\ganttnewline\n")
    for node in node_set:
        latex_file.write("  \\ganttgroup{switch %d port %d}{0}{25}\\ganttnewline\n" % (node[0], node[1]))
        letter = ord("a")
        for num_flow in range(0, len(flow)):
            if node in flow[num_flow].nodes:
                for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
                    latex_file.write("    \\ganttgroup{gate %d}{\\csname %s \\endcsname}{\\csname %s \\endcsname}\n" % (num_flow, node_gate_open_offset(num_flow, node, num_frame), node_gate_close_offset(num_flow, node, num_frame)))
                latex_file.write("    \\ganttmilestone{%d}{%d}\n" % (num_flow, flow[num_flow].e2e_time-1))
#        window = 0
#        for gate in flow_order[node]:
#            latex_file.write("    \\ganttgroup{gate %d}{\\csname %s \\endcsname}{\\csname %s \\endcsname}\n" % (window, node_gate_open_offset(gate, node, window), node_gate_close_offset(gate, node, window)))
#            gate += 1
        latex_file.write("  \\ganttnewline\n")
        for num_flow in range(0, len(flow)):
            if node in flow[num_flow].nodes:
                for num_frame in range(0, int(hyperperiod/flow[num_flow].period)):
                    latex_file.write("    \\ganttbar[bar/.append style={fill=col%s}]{%d}{\\csname %s \\endcsname}{\\csname %s \\endcsname}\n" % (chr(letter), num_frame, frame_node_start(num_flow, num_frame, node), frame_node_complete(num_flow, num_frame, node)))
            letter += 1
        latex_file.write("  \\ganttnewline\n")
    latex_file.write("\\end{ganttchart}\n")
    latex_file.write("\\end{document}\n")
    latex_file.close()

def main():
#    if len(sys.argv) != 2:
#        print("usage:", sys.argv[0], "<binaryfile>")
#        sys.exit()
#    filename = sys.argv[1]
    #flow = genera_datos_iniciales()
    flow = set_data_to_schedule()
    node_set = set()
    for num_flow in range(0, len(flow)):
        print("/* **** flow", num_flow, "**** */")
        flow[num_flow].print()
        for node in flow[num_flow].nodes:
            node_set.add(node)

    # hyperperiod = gate cycle time = planificación de gates que se repite con el tiempo
    hyperperiod = calculate_hyperperiod(flow) # l.c.m. flow periods
    print("\n/* Hyperperiod:", hyperperiod, "microseconds */")
    flow_order = calculate_flow_order(hyperperiod, flow, node_set)
    lp_file = sys.stdout

    # Time to schedule may be > than the hyperperiod...
    #  a) if the end-to-end delay is greater than the frame period
    #  b) if the flow is scheduled to start with a large enough offset
    time_to_schedule = hyperperiod
    max_hops = 0
    for num_flow in range(0, len(flow)):
        extra_scheduling_time = flow[num_flow].e2e_time
        time_to_schedule = max(time_to_schedule, hyperperiod + extra_scheduling_time)
    num_hyperperiods = ceil(time_to_schedule / hyperperiod)
    # TODO: hacer algo para permitir sistemas no planificables para ver cuál es el flujo que da problemas?
    # TODO: no es trivial. Habría que ampliar periodos de flujos, no solo end-to-end times
    print("/* Number of hyperperiods to anayze:", num_hyperperiods, "*/\n")

    # calculate the number of frames that go through each bridge/port
    # this method assumes one window per frame
    max_windows = 0
    for node in node_set:
        if len(flow_order[node]) > max_windows:
            max_windows = len(flow_order[node])
        print("/*", len(flow_order[node]), "windows in node/port", node, "with (Flow, Frame) order:", flow_order[node], "*/")
        #print("/* Node/port", node, "has", len(flow_order[node]), "windows with */")
        #print("/* (Flow, Frame) order:", flow_order[node], "*/")
    print("/* Maximum number of windows per port:", max_windows, "*/")

    #generate_xml_template(hyperperiod, flow)
    #generate_latex_template(hyperperiod, flow, node_set, flow_order)

    # objective
    generate_max_time_margin_objective(lp_file, hyperperiod, flow)

    # flow constraints
    generate_offset_constraints(lp_file, flow)
    generate_first_ready_constraints(lp_file, hyperperiod, flow)
    generate_frame_start_constraints(lp_file, hyperperiod, flow)
    generate_frame_completion_constraints(lp_file, hyperperiod, flow)
    generate_path_constraints(lp_file, hyperperiod, flow)
    generate_frame_order_constraints(lp_file, hyperperiod, flow)

    # gate constraints
    generate_gate_constraints(lp_file, flow, hyperperiod, node_set, flow_order)

    # frame to window association
    generate_frame_start_offset_constraints(lp_file, hyperperiod, num_hyperperiods, flow)
    generate_gate_transmission_start_constraints(lp_file, hyperperiod, flow)
    generate_gate_transmission_complete_constraints(lp_file, hyperperiod, flow)

    # delay and jitter computation
    generate_time_margin_constraints(lp_file, hyperperiod, flow)
    generate_frame_delay_constraints(lp_file, hyperperiod, flow)
    generate_jitter_constraints(lp_file, hyperperiod, flow)

    #generate_flow_e2e_constraints(lp_file, hyperperiod, flow) # Required? Positive margin should suffice
    generate_binary_variables(lp_file, hyperperiod, num_hyperperiods, flow, node_set, flow_order)
    # WARNING: free variables allow (negative) solutions for infeasible schedules. Currently it does not work!
    # Remove the generate_free_variables call to get feasible solutions
    #generate_free_variables(lp_file, hyperperiod, flow)



if __name__ == "__main__":
    sys.exit(main())
