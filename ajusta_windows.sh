#!/bin/sh

# Uso: cat modelo.result | ./ajusta_windows.sh

# filtra y ordena <stream, frame, bridge, port, complete, ready>
#sort | grep -E 'ready|complete' | sed 's/st//g;s/fr/ /g;s/sw/ /g;s/pt/ /g;s/complete/ /g;s/  */ /g;s/.*ready \(.*\)/ \1#/g' | tr -d '\n' | tr '#' '\n' | python3 ajusta_windows.py

# filtra y ordena <stream, frame, bridge, port, queued, open, complete, close>
sort | grep -E 'ready|complete|open|close' | sed 's/\(.*\)gt\(.*\)open/\2 \1open/g;s/\(.*\)gt\(.*\)close/\2 \1close/g;s/st//g;s/fr/ /g;s/sw/ /g;s/pt/ /g;s/w/ /g;s/  */ /g;s/ready/ 1ready/g;s/openOffset/ 2open/g;s/complete/ 3complete/g;s/closeOffset/ 4close/g' | sort | sed 's/ 1ready//g;s/.*2open//g;s/.*3complete//g;s/.*4close\(.*\)/\1#/g' | tr -d '\n' | tr '#' '\n' | python3 ajusta_windows.py

